require_relative '../../lib/geo/constants'
module Converters
  def meters_to_nmi(meters)
    (meters / Geo::METERS_IN_NMI).round
  end
end