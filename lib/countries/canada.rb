module Canada
  CODE = :CA

  def self.regions
    { :ARCTIC => 37, :YUKON => 35, :NUNAVUT => 36, :PACIFIC => 31, :PRAIRIES => 32, :ONTARIO => 33, :ATLANTIC => 34 }
  end

end