module Math
  def self.to_rad(degrees)
    degrees * Math::PI / 180
  end

  def self.to_deg(radians)
    radians * 180 / Math::PI
  end
end