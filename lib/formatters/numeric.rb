module NumberFormat
  def with_leading_zero(coordinate)
    '%02d' % coordinate
  end
end