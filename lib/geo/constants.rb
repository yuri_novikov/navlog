module Geo
  NORTH = :N
  SOUTH = :S
  WEST = :W
  EAST = :E

  #in meters
  EARTH_RADIUS = 6372795

  METERS_IN_NMI = 1852.00
end