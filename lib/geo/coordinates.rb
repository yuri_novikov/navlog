# encoding: UTF-8
require_relative '../formatters/numeric'
require_relative '../ext/math'
require_relative 'constants'

require 'mongoid'

module Geo
  class Coordinate
    include NumberFormat, Mongoid::Document

    field :direction, :type => Symbol
    field :degree, :type => Integer
    field :minute, :type => Integer
    field :second, :type => Integer

    def initialize(direction, degree, minute, second)
      unless (0..59).include?(minute) && (0..59).include?(second)
        raise ArgumentError, 'Minutes and seconds should be within 00 and 59 range'
      end
      super({:direction => direction, :degree => degree, :minute => minute, :second => second})
    end


    def to_s
      "#{direction} #{with_leading_zero(degree)}° #{with_leading_zero(minute)}′ #{with_leading_zero(second)}″"
    end

    # return a decimal representation of the coordinate that is rounded to 4 digits precision
    # Formula used: DDD = DD + MM/60 + SS/3600. For South latitude and West longitude the value is negative
    def as_decimal
      decimal_coordinate = (Float(degree) + Float(minute.to_s) / 60 + Float(second.to_s) / 3600).round(4)
      [SOUTH, WEST].include?(direction) ? -decimal_coordinate : decimal_coordinate
    end

    protected
    def self.create_from_decimal(direction, coordinate_decimal)
      degree = coordinate_decimal.to_i
      minutes_and_seconds = (coordinate_decimal - degree) * 60
      minute = minutes_and_seconds.to_i
      second = ((minutes_and_seconds - minute) * 60).round
      if [EAST, WEST].include?(direction)
        Longitude.new(direction, degree, minute, second)
      else
        Latitude.new(direction, degree, minute, second)
      end
    end

    def self.create_from_string(coordinate_as_string, fd_format_validator, cfs_format_validator, coordinate_constructor)
      if fd_format_validator.call(coordinate_as_string)
        direction = coordinate_as_string[-1].to_sym
        coordinate_decimal = coordinate_as_string.chop.to_f
        self.create_from_decimal(direction, coordinate_decimal)
      elsif cfs_format_validator.call(coordinate_as_string)
        direction = coordinate_as_string.chr.to_sym
        coordinate_array = coordinate_as_string.slice(1..-1).split(' ')
        degree = coordinate_array[0].to_i
        minute = coordinate_array[1].to_i
        second = coordinate_array[2].to_i
        coordinate_constructor.call(direction, degree, minute, second)
      else
        raise ArgumentError, 'Unsupported coordinate input format'
      end
    end
  end

  class Latitude < Coordinate
    def initialize(direction, degree, minute, second)
      unless [NORTH, SOUTH].include?(direction)
        raise ArgumentError, "Latitude direction should be either #{NORTH} or #{SOUTH}"
      end
      unless (0..90).include?(degree)
        raise ArgumentError, 'Latitude should be within 00 and 90 degrees'
      end
      if degree == 90 && (minute > 0 || second > 0)
        raise ArgumentError, 'Latitude value can\'t be more that 90 degrees'
      end
      super(direction, degree, minute, second)
    end

    def self.from_dec(latitude)
      direction = (latitude > 0) ? NORTH : SOUTH
      create_from_decimal(direction, latitude.abs)
    end

    # The following formats are supported:
    #
    # 18.4N (a decimal coordinate with single decimal digit followed by direction)
    # S18 00 11 (direction with degree, minute, second separated by whitespaces)
    def self.from_str(latitude_as_string)
      fd_latitude_format = Proc.new { |longitude_as_s| (longitude_as_s=~/\d{,2}.\d[NS]/) == 0 }
      cfs_latitude_format = Proc.new { |longitude_as_s| (longitude_as_s=~/[NS]\d{,2}\s\d{2}\s\d{2}/) == 0 }
      latitude_constructor = Proc.new {|direction, degree, minute, second| Latitude.new(direction, degree, minute, second) }
      create_from_string(latitude_as_string, fd_latitude_format, cfs_latitude_format, latitude_constructor)
    end
  end

  class Longitude < Coordinate
    def initialize(direction, degree, minute, second)
      unless [EAST, WEST].include?(direction)
        raise ArgumentError, "Longitude direction should be either #{EAST} or #{WEST}"
      end
      unless (0..180).include?(degree)
        raise ArgumentError, 'Longitude should be within 00 and 180 degrees'
      end
      if degree == 180 && (minute > 0 || second > 0)
        raise ArgumentError, 'Longitude value can\'t be more that 180 degrees'
      end
      super(direction, degree, minute, second)
    end

    def self.from_dec(longitude)
      direction = (longitude > 0) ? EAST : WEST
      create_from_decimal(direction, longitude.abs)
    end

    # The following formats are supported:
    #
    # 128.4E (a decimal coordinate with single decimal digit followed by direction)
    # W128 00 11 (direction with degree, minute, second separated by whitespaces)
    def self.parse(longitude_as_string)
      fd_longitude_format = Proc.new { |longitude_as_s| (longitude_as_s=~/\d{,3}.\d[WE]/) == 0 }
      cfs_longitude_format = Proc.new { |longitude_as_s| (longitude_as_s=~/[WE]\d{,3}\s\d{2}\s\d{2}/) == 0 }
      longitude_constructor = Proc.new {|direction, degree, minute, second| Longitude.new(direction, degree, minute, second) }
      create_from_string(longitude_as_string, fd_longitude_format, cfs_longitude_format, longitude_constructor)
    end
  end

end