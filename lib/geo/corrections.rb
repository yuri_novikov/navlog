# encoding: UTF-8
require_relative '../formatters/numeric'
require_relative 'constants'

require 'mongoid'

module Geo
  # Represents magnetic variation.
  class Variation
    include NumberFormat, Mongoid::Document

    field :direction, :type => Symbol
    field :degree, :type => Integer

    def initialize(direction, degree)
      unless direction == EAST || direction == WEST
        raise ArgumentError, "Variation direction should be either #{EAST} or #{WEST}"
      end
      unless (0..180).include?(degree)
        raise ArgumentError, 'Variation should be within 00 and 180 degrees'
      end
      super({:direction => direction, :degree => degree})
    end

    # The correct format is: 17E, 5W. That is, one or two digits followed by a direction symbol.
    def self.from_str(variation_str)
      if (variation_str=~/\d{,2}[EW]/) == 0
        direction = variation_str[-1].to_sym
        correction = variation_str.chop.to_f
        new(direction, correction)
      else
        raise ArgumentError, 'Unsupported variation input format'
      end
    end

    def to_s
      "#{with_leading_zero(degree)}° #{direction}"
    end

    # East is least, West is best. We are going to subtract easterly variation and add westerly one.
    # So we return easterly variations as negative values.
    def as_decimal
      (direction == EAST)? -degree : degree
    end
  end
end