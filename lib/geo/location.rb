# encoding: UTF-8
require_relative 'coordinates'

require 'mongoid'

module Geo

  # Represents a geo point with given coordinates - latitude and longitude.
  class Location
    include Mongoid::Document

    embeds_one :latitude, class_name: 'Geo::Latitude'
    embeds_one :longitude, class_name: 'Geo::Longitude'

    def initialize(latitude, longitude)
      super({:latitude => latitude, :longitude => longitude})
    end

    def self.from_str(latitude_as_s, longitude_as_s)
      new(Latitude.from_str(latitude_as_s), Longitude.parse(longitude_as_s))
    end

    # Using the Haversine formula to determine the distance between two locations.
    #
    # Haversine formula: 	a = sin²(Δφ/2) + cos(φ1).cos(φ2).sin²(Δλ/2);
    # where a — is the square of half the chord length between the points,
    # c = 2.atan2(√a, √(1−a)) — the angular distance in radians,
    # d = R.c — the distance, φ — latitude, λ — longitude, R — earth’s radius (radius = 6372795 m).
    # note that angles need to be in radians to pass to trig functions!
    #
    # @return great circle line distance_nmi as the closest integer value in meters
    def great_circle_line_distance_to(another_location)
      d_lat = Math.to_rad(another_location.latitude.as_decimal - latitude.as_decimal)
      d_lon = Math.to_rad(another_location.longitude.as_decimal - longitude.as_decimal)
      lat1 = Math.to_rad(latitude.as_decimal)
      lat2 = Math.to_rad(another_location.latitude.as_decimal)

      a = Math.sin(d_lat/2)**2 + Math.sin(d_lon/2)**2 * Math.cos(lat1) * Math.cos(lat2)
      c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
      (c * EARTH_RADIUS).round
    end

    # Determines the initial bearing from one location to another to follow the great cirle line arc.
    #
    # Formula: θ = atan2(sin(Δλ).cos(φ2), cos(φ1).sin(φ2) − sin(φ1).cos(φ2).cos(Δλ));
    # where φ — latitude, λ — longitude.
    #
    # Since atan2 returns values in the range -π ... +π (that is, -180° ... +180°),
    # to normalise the result to a compass bearing (in the range 0° ... 360°,
    # with −ve values transformed into the range 180° ... 360°),
    # convert to degrees and then use (θ+360) % 360, where % is modulo.
    #
    # @return initial bearing as the closest integer value
    def initial_bearing_to(another_location)
      d_lon = Math.to_rad(another_location.longitude.as_decimal - longitude.as_decimal)
      lat1 = Math.to_rad(latitude.as_decimal)
      lat2 = Math.to_rad(another_location.latitude.as_decimal)

      y = Math.sin(d_lon) * Math.cos(lat2)
      x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(d_lon)
      theta = Math.to_deg(Math.atan2(y, x))
      ((theta + 360) % 360).round
    end
  end

end