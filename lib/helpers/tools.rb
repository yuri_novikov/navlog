# encoding: UTF-8
module CompassTools
  def western?(bearing)
    (180..359).include?(bearing)
  end

  def eastern?(bearing)
    (0..179).include?(bearing)
  end
end

module AltitudeTools
  # all values below in feet
  CRUISE_ALT_INCREMENT = 500
  MINIMAL_VFR_CRUISE_LEVEL = 3500
  VFR_CRUISE_LEVEL_LOWER_LIMIT = 3000

  # @return cruise altitude in thousands feet less the 500 feet increment
  def cruise_alt_thousand_feet(altitude)
    unless correct_vfr_cruise_level?(altitude)
      raise ArgumentError, 'Altitude is not a VFR cruise flight level and should be provided as a thousands of feet number plus 500 feet starting from 3500 feet'
    end
    (altitude - CRUISE_ALT_INCREMENT) / 1000
  end

  # VFR cruise levels should be in thousands feet with a 500 feet increment starting from 3500 feet
  def correct_vfr_cruise_level?(altitude)
    (altitude % CRUISE_ALT_INCREMENT == 0) && (altitude >= MINIMAL_VFR_CRUISE_LEVEL)
  end

  # Under 3000 feet there's no cruising altitude restriction.
  def vfr_cruise_level_required?(altitude)
    altitude > VFR_CRUISE_LEVEL_LOWER_LIMIT
  end
end