# encoding: UTF-8
require_relative '../../lib/converters/distance'
require_relative '../../lib/helpers/tools'
require_relative '../../lib/geo/location'

require 'mongoid'

module Route
  class Leg
    include Converters, CompassTools, AltitudeTools, Mongoid::Document

    embeds_one :from, class_name: 'Geo::Location'
    embeds_one :to, class_name: 'Geo::Location'

    field :altitude, :type => Integer
    field :moca, :type => Integer

    def initialize(from, to)
      super({:from => from, :to => to})
    end

    def true_track
      from.location.initial_bearing_to(to.location)
    end

    # Applying leg variation to the true track.
    def magnetic_track
      true_track + leg_variation
    end

    # Returns leg variation as a mean value of the variation endpoints.
    def leg_variation
      (from.variation.as_decimal + to.variation.as_decimal) / 2
    end

    def distance_nmi
      meters_to_nmi(from.location.great_circle_line_distance_to(to.location))
    end

    # Sets the altitude taking into account the VFR cruise altitude selection rules and MOCA
    def altitude=(altitude)
      if moca && altitude <= moca
        raise ArgumentError, "Selected altitude is below safety limits for this leg: #{moca}"
      end
      if vfr_cruise_level_required?(altitude)
        alt_thousands_feet = cruise_alt_thousand_feet(altitude)
        if eastern?(magnetic_track) && alt_thousands_feet.even?
          raise ArgumentError, 'For a magnetic track between 0 and 179 only cruising altitudes in odd thousands plus 500 feet are allowed'
        end
        if western?(magnetic_track) && alt_thousands_feet.odd?
          raise ArgumentError, 'For a magnetic track between 180 and 359 only cruising altitudes in even thousands plus 500 feet are allowed'
        end
      end
      super(altitude)
    end
  end
end