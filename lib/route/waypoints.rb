require_relative '../../lib/geo/corrections'
require_relative '../../lib/geo/location'

require 'mongoid'

module Route
  class Waypoint
    include Mongoid::Document
    store_in collection: 'nav_objects'

    field :country
    field :subcountry

    embeds_one :location, class_name: 'Geo::Location'
    embeds_one :variation, class_name: 'Geo::Variation'

    def initialize(location, variation = nil)
      super({:location => location, :variation => variation})
    end

    def self.from_str(latitude_as_s, longitude_as_s, variation_str)
      if variation_str.nil?
        new(Geo::Location.from_str(latitude_as_s, longitude_as_s))
      else
        new(Geo::Location.from_str(latitude_as_s, longitude_as_s), Geo::Variation.from_str(variation_str))
      end
    end
  end

  class FDStation < Waypoint
    include Mongoid::Document
    field :iata

    def initialize(location, variation = nil)
      super(location, variation)
    end

    def self.from_str(iata, latitude_as_s, longitude_as_s, variation_str)
      fd_station = if variation_str.nil?
        new(Geo::Location.from_str(latitude_as_s, longitude_as_s))
      else
        new(Geo::Location.from_str(latitude_as_s, longitude_as_s), Geo::Variation.from_str(variation_str))
      end
      fd_station.iata = iata
      fd_station
    end

  end

  class Airport < Waypoint
    attr_accessor :name, :iata, :icao, :elevation, :country, :city

    def initialize(name, iata, location, variation = nil)
      super(location, variation)
      @name = name
      @iata = iata
    end
  end
end