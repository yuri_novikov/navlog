require 'mongoid'
require 'test/unit'

class MongoTest < Test::Unit::TestCase
  def setup
    Mongoid.load!('../conf/mongoid_test.yml', :test)
  end

  def teardown
    Mongoid.purge!
  end
end