require_relative '../../lib/route/waypoints'
require_relative '../../lib/route/leg'
require_relative '../../persistence_test/framework/mongo_test_support'

class LegCRUDTest < MongoTest
  include Route

  def setup
    super()
    @leg = Leg.new(
        Waypoint.from_str('N49 04 26', 'W123 00 27', '17E'),
        Waypoint.from_str('N49 04 56', 'W125 46 21', '17E')
    )
    @leg.altitude = 4500
  end

  def test_save
    assert @leg.save
    assert_equal 1, Leg.count
  end

end
