require_relative '../../lib/route/waypoints'
require_relative '../../lib/countries/canada'
require_relative '../../persistence_test/framework/mongo_test_support'

class WaypointCRUDTest < MongoTest
  include Route

  def setup
    super()
    @waypoint = Waypoint.from_str('N49 04 26', 'W123 00 27', '17E')
    @waypoint.country = Canada::CODE
    @waypoint.subcountry = Canada::regions[:PACIFIC]
    assert @waypoint.save
  end

  def test_assert_count
    assert_equal 1, Waypoint.count
  end

  def test_load
    waypoint = Waypoint.where(country: Canada::CODE).first
    assert_equal Canada::regions[:PACIFIC], waypoint.subcountry
  end
end
