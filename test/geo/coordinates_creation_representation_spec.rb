# encoding: UTF-8
require 'rspec'

require_relative '../../lib/geo/coordinates'

describe Geo::Latitude, 'creation and representation' do

  let(:czbb_lat) { Geo::Latitude.from_str('N49 04 26') }

  it 'should return degrees, minutes and seconds' do
    czbb_lat.degree.should == 49
    czbb_lat.minute.should == 4
    czbb_lat.second.should == 26
  end

  it 'should return N 49° 04′ 26″ as a textual representation' do
    czbb_lat.to_s.should == 'N 49° 04′ 26″'
  end

  it 'should return 49.0739 as a representation in degrees decimal' do
    czbb_lat.as_decimal.should == 49.0739
  end

  it 'should raise an error when degrees > 90 or degrees < 0' do
    expect { Geo::Latitude.new(Geo::NORTH, 120, 0, 0) }.to raise_error(ArgumentError, /Latitude should be within 00 and 90 degrees/)
    expect { Geo::Latitude.new(Geo::NORTH, -120, 0, 0) }.to raise_error(ArgumentError, /Latitude should be within 00 and 90 degrees/)
  end

  it 'should raise an error when minutes or seconds > 60 or minutes or seconds < 0' do
    expect { Geo::Latitude.new(Geo::NORTH, 42, 95, 0) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Latitude.new(Geo::NORTH, 42, 32, 88) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Latitude.new(Geo::NORTH, 42, -95, 0) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Latitude.new(Geo::NORTH, 42, -32, 88) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
  end

  it 'should raise an error when latitude > 90' do
    expect { Geo::Latitude.new(Geo::NORTH, 90, 10, 0) }.to raise_error(ArgumentError, /Latitude value can't be more that 90 degrees/)
  end

  it "should raise an error when latitude direction is #{Geo::EAST} or #{Geo::WEST}" do
    expect { Geo::Latitude.new(Geo::EAST, 58, 10, 0) }.to raise_error(ArgumentError, /Latitude direction should be either #{Geo::NORTH} or #{Geo::SOUTH}/)
    expect { Geo::Latitude.new(Geo::WEST, 58, 10, 0) }.to raise_error(ArgumentError, /Latitude direction should be either #{Geo::NORTH} or #{Geo::SOUTH}/)
  end

  it 'should return N 49° 04′ 26″ when creating from 49.0739 decimal representation' do
    latitude_from_decimal = Geo::Latitude.from_dec(49.0739)
    latitude_from_decimal.direction.should == czbb_lat.direction
    latitude_from_decimal.degree.should == czbb_lat.degree
    latitude_from_decimal.minute.should == czbb_lat.minute
    latitude_from_decimal.second.should == czbb_lat.second
    latitude_from_decimal.to_s.should == czbb_lat.to_s
  end
end

describe Geo::Longitude, 'creation and representation' do

  let(:czbb_lon) { Geo::Longitude.parse('W123 00 27') }

  it 'should return degrees, minutes and seconds' do
    czbb_lon.degree.should == 123
    czbb_lon.minute.should == 0
    czbb_lon.second.should == 27
  end

  it 'should return W 123° 00′ 27″ as a textual representation' do
    czbb_lon.to_s.should == 'W 123° 00′ 27″'
  end

  it 'should return -123.0075 as a representation in degrees decimal' do
    czbb_lon.as_decimal.should == -123.0075
  end

  it 'should raise an error when degrees > 180 or degrees < 0' do
    expect { Geo::Longitude.new(Geo::EAST, 190, 0, 0) }.to raise_error(ArgumentError, /Longitude should be within 00 and 180 degrees/)
    expect { Geo::Longitude.new(Geo::EAST, -10, 0, 0) }.to raise_error(ArgumentError, /Longitude should be within 00 and 180 degrees/)
  end

  it 'should raise an error when minutes or seconds > 60 or minutes or seconds < 0' do
    expect { Geo::Longitude.new(Geo::EAST, 42, 95, 0) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Longitude.new(Geo::EAST, 42, 32, 88) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Longitude.new(Geo::EAST, 42, -95, 0) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
    expect { Geo::Longitude.new(Geo::EAST, 42, -32, 88) }.to raise_error(ArgumentError, /Minutes and seconds should be within 00 and 59 range/)
  end

  it 'should raise an error when latitude > 180' do
    expect { Geo::Longitude.new(Geo::EAST, 180, 10, 0) }.to raise_error(ArgumentError, /Longitude value can't be more that 180 degrees/)
  end

  it "should raise an error when latitude direction is #{Geo::NORTH} or #{Geo::SOUTH}" do
    expect { Geo::Longitude.new(Geo::NORTH, 58, 10, 0) }.to raise_error(ArgumentError, /Longitude direction should be either #{Geo::EAST} or #{Geo::WEST}/)
    expect { Geo::Longitude.new(Geo::SOUTH, 58, 10, 0) }.to raise_error(ArgumentError, /Longitude direction should be either #{Geo::EAST} or #{Geo::WEST}/)
  end

  it 'should return W 123° 00′ 27″ when creating from -123.0075 decimal representation' do
    longitude_from_decimal = Geo::Longitude.from_dec(-123.0075)
    longitude_from_decimal.direction.should == czbb_lon.direction
    longitude_from_decimal.degree.should == czbb_lon.degree
    longitude_from_decimal.minute.should == czbb_lon.minute
    longitude_from_decimal.second.should == czbb_lon.second
    longitude_from_decimal.to_s.should == czbb_lon.to_s
  end
end