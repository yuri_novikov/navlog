# encoding: UTF-8
require 'rspec'

require_relative '../../lib/geo/location'

describe Geo::Location, 'distance and initial bearing calculations between CZBB (Boundary Bay) and CYAZ (Tofino) and back.
  Distances should be equal, initial bearings should be reciprocative to each other' do
  let(:czbb) {
    Geo::Location.from_str('N49 04 26', 'W123 00 27')
  }
  let(:cyaz) {
    Geo::Location.from_str('N49 04 56', 'W125 46 21')
  }

  DISTANCE_CZBB_CYAZ = 201439

  it 'should return the distance of 201439 as the closest integer value in meters CZBB->CYAZ' do
    czbb.great_circle_line_distance_to(cyaz).should == DISTANCE_CZBB_CYAZ
  end

  it 'should return the distance of 201439 as the closest integer value in meters CYAZ->CZBB' do
    cyaz.great_circle_line_distance_to(czbb).should == DISTANCE_CZBB_CYAZ
  end

  it 'should return the initial bearing of 271 as the closest integer value in degrees CZBB->CYAZ' do
    czbb.initial_bearing_to(cyaz).should == 271
  end

  it 'should return the initial bearing of 89 (reciprocative of 271) as the closest integer value in degrees CYAZ->CZBB' do
    cyaz.initial_bearing_to(czbb).should == 89
  end
end