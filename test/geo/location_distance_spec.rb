# encoding: UTF-8
require 'rspec'

require_relative '../../lib/geo/location'

describe Geo::Location, 'distance calculations between locations with given coordinates' do
  it 'should return the great circle line distance between two coordinates as the closest integer value in meters' do
    location1 = Geo::Location.from_str('N77 09 14', 'W139 23 52')
    location2 = Geo::Location.from_str('S77 10 49', 'W139 33 04')
    location1.great_circle_line_distance_to(location2).should == 17166018
  end

  it 'should return the great circle line distance between two coordinates as the closest integer value in meters' do
    location1 = Geo::Location.from_str('N77 09 14', 'E120 23 52')
    location2 = Geo::Location.from_str('N77 10 49', 'E129 33 04')
    location1.great_circle_line_distance_to(location2).should == 225916
  end

  it 'should return the great circle line distance between two coordinates as the closest integer value in meters' do
    location1 = Geo::Location.from_str('N77 09 14', 'W120 23 52')
    location2 = Geo::Location.from_str('N77 10 49', 'E129 33 04')
    location1.great_circle_line_distance_to(location2).should == 2332665
  end
end