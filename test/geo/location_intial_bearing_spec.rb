# encoding: UTF-8
require 'rspec'

require_relative '../../lib/geo/location'

describe Geo::Location, 'initial bearing calculations between locations with given coordinates' do
  it 'should return the initial bearing between locations as the closest integer value in degrees' do
    location1 = Geo::Location.from_str('N77 09 14', 'W139 23 52')
    location2 = Geo::Location.from_str('S77 10 49', 'W139 33 04')
    location1.initial_bearing_to(location2).should == 180
  end

  it 'should return the initial bearing between locations as the closest integer value in degrees' do
    location1 = Geo::Location.from_str('N77 09 14', 'E120 23 52')
    location2 = Geo::Location.from_str('N77 10 49', 'E129 33 04')
    location1.initial_bearing_to(location2).should == 85
  end

  it 'should return the initial bearing between locations as the closest integer value in degrees' do
    location1 = Geo::Location.from_str('N77 09 14', 'W120 23 52')
    location2 = Geo::Location.from_str('N77 10 49', 'E129 33 04')
    location1.initial_bearing_to(location2).should == 324
  end
end