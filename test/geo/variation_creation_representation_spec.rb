# encoding: UTF-8
require 'rspec'

require_relative '../../lib/geo/corrections'

describe Geo::Variation, 'creation and representation' do

  let(:czbb_variation) { Geo::Variation.from_str('17E') }

  it 'should return degrees' do
    czbb_variation.degree.should == 17
  end

  it 'should return 17° E as a textual representation' do
    czbb_variation.to_s.should == '17° E'
  end

  it 'should raise an error when degrees > 180 or degrees < 0' do
    expect { Geo::Variation.new(Geo::EAST, 190) }.to raise_error(ArgumentError, /Variation should be within 00 and 180 degrees/)
    expect { Geo::Variation.new(Geo::EAST, -10) }.to raise_error(ArgumentError, /Variation should be within 00 and 180 degrees/)
  end

  it "should raise an error when variation direction is #{Geo::NORTH} or #{Geo::SOUTH}" do
    expect { Geo::Variation.new(Geo::NORTH, 58) }.to raise_error(ArgumentError, /Variation direction should be either #{Geo::EAST} or #{Geo::WEST}/)
    expect { Geo::Variation.new(Geo::SOUTH, 60) }.to raise_error(ArgumentError, /Variation direction should be either #{Geo::EAST} or #{Geo::WEST}/)
  end

  it 'should return a negative decimal value for an easterly variation' do
    Geo::Variation.from_str('17E').as_decimal.should < 0
  end

  it 'should return a positive decimal value for a westerly variation' do
    Geo::Variation.from_str('17W').as_decimal.should > 0
  end

end