# encoding: UTF-8
require 'rspec'

require_relative '../../lib/route/waypoints'
require_relative '../../lib/route/leg'

describe Route::Leg, 'VFR cruising altitude restrictions' do

  let(:czbb_to_cyaz) {
    Route::Leg.new(
        Route::Waypoint.from_str('N49 04 26', 'W123 00 27', '17E'),
        Route::Waypoint.from_str('N49 04 56', 'W125 46 21', '17E')
    )
  }
  let(:cyaz_to_czbb) {
    Route::Leg.new(
        Route::Waypoint.from_str('N49 04 56', 'W125 46 21', '17E'),
        Route::Waypoint.from_str('N49 04 26', 'W123 00 27', '17E')
    )
  }

  it 'should accept any altitude regardless of magnetic track if it\'s less than 3000 feet' do
    czbb_to_cyaz.altitude = 1500
    czbb_to_cyaz.altitude.should == 1500

    czbb_to_cyaz.altitude = 2400
    czbb_to_cyaz.altitude.should == 2400

    czbb_to_cyaz.altitude = 2900
    czbb_to_cyaz.altitude.should == 2900
  end

  it 'should accept even thousand feet cruise level altitudes + 500 feet for westerly magnetic tracks between 180 and 359' do
    czbb_to_cyaz.altitude = 4500
    czbb_to_cyaz.altitude.should == 4500

    czbb_to_cyaz.altitude = 6500
    czbb_to_cyaz.altitude.should == 6500
  end

  it 'should not accept odd thousand feet cruise level altitudes + 500 feet for westerly magnetic tracks between 180 and 359' do
    expect { czbb_to_cyaz.altitude = 3500 }.to raise_error(ArgumentError,
            /For a magnetic track between 180 and 359 only cruising altitudes in even thousands plus 500 feet are allowed/)

    expect { czbb_to_cyaz.altitude = 7500 }.to raise_error(ArgumentError,
            /For a magnetic track between 180 and 359 only cruising altitudes in even thousands plus 500 feet are allowed/)
  end

  it 'should accept only odd thousand feet cruise level altitudes + 500 feet for easterly magnetic tracks between 0 and 179' do
    cyaz_to_czbb.altitude = 3500
    cyaz_to_czbb.altitude.should == 3500

    cyaz_to_czbb.altitude = 5500
    cyaz_to_czbb.altitude.should == 5500
  end

  it 'should not accept even thousand feet cruise level altitudes + 500 feet for easterly magnetic tracks between 0 and 179' do
    expect { cyaz_to_czbb.altitude = 4500 }.to raise_error(ArgumentError,
             /For a magnetic track between 0 and 179 only cruising altitudes in odd thousands plus 500 feet are allowed/)

    expect { cyaz_to_czbb.altitude = 6500 }.to raise_error(ArgumentError,
             /For a magnetic track between 0 and 179 only cruising altitudes in odd thousands plus 500 feet are allowed/)
  end

  it 'should not accept any altitude between 3000 and 3500 because VFR cruise altitude start from 3500 if flying higher than 3000 feet' do
    expect { czbb_to_cyaz.altitude = 3200 }.to raise_error(ArgumentError,
            /Altitude is not a VFR cruise flight level and should be provided as a thousands of feet number plus 500 feet starting from 3500 feet/)
    expect { cyaz_to_czbb.altitude = 3300 }.to raise_error(ArgumentError,
            /Altitude is not a VFR cruise flight level and should be provided as a thousands of feet number plus 500 feet starting from 3500 feet/)
  end

  it 'should not accept any altitude that is not with 500 feet increment as a VFR flight level (valid: 3500, 4500, etc)' do
    expect { czbb_to_cyaz.altitude = 4300 }.to raise_error(ArgumentError,
             /Altitude is not a VFR cruise flight level and should be provided as a thousands of feet number plus 500 feet starting from 3500 feet/)
    expect { cyaz_to_czbb.altitude = 5300 }.to raise_error(ArgumentError,
             /Altitude is not a VFR cruise flight level and should be provided as a thousands of feet number plus 500 feet starting from 3500 feet/)
  end

  it 'should not accept any altitude which is below minimum obstacle clearing altitude' do
    czbb_to_cyaz.moca = 8000
    expect { czbb_to_cyaz.altitude = 7500 }.to raise_error(ArgumentError,
            /Selected altitude is below safety limits for this leg: 8000/)
  end
end