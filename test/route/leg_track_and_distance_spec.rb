# encoding: UTF-8
require 'rspec'

require_relative '../../lib/route/waypoints'
require_relative '../../lib/route/leg'

describe Route::Leg, 'true and magnetic track calculations' do

  let(:czbb_to_cyaz) {
    Route::Leg.new(
        Route::Waypoint.from_str('N49 04 26', 'W123 00 27', '17E'),
        Route::Waypoint.from_str('N49 04 56', 'W125 46 21', '17E')
    )
  }
  let(:cyaz_to_czbb) {
    Route::Leg.new(
        Route::Waypoint.from_str('N49 04 56', 'W125 46 21', '17E'),
        Route::Waypoint.from_str('N49 04 26', 'W123 00 27', '17E')
    )
  }

  it 'should return 271 as a true track from CZBB->CYAZ' do
    czbb_to_cyaz.true_track.should == 271
  end

  it 'should return 254 as a magnetic track from CZBB->CYAZ (true track less easterly variation)' do
    czbb_to_cyaz.magnetic_track.should == 254
  end

  it 'should return 89 as a true track from CYAZ->CZBB' do
    cyaz_to_czbb.true_track.should == 89
  end

  it 'should return 72 as a magnetic track from CYAZ->CZBB (true track less easterly variation)' do
    cyaz_to_czbb.magnetic_track.should == 72
  end
  
  it 'should return the distance of 109 as the closest integer value in nautical miles CYAZ->CZBB' do
    cyaz_to_czbb.distance_nmi.should == 109
  end

  it 'should return the distance of 109 as the closest integer value in nautical miles CZBB->CYAZ' do
    czbb_to_cyaz.distance_nmi.should == 109
  end

end